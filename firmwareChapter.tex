 \chapter{Firmware}
This chapter includes information about re-programming the FPGA and some aspects of the VMM Readout/Configuration Firmware of the GPVMM board. Be advised that there are two versions of the firmware, with or without the support of CTF module. The firmware's .bin files can be downloaded from \url{https://gitlab.cern.ch/NSWelectronics/vmm_boards_firmware} and the branch that contains the .bin files for the GPVMM is \emph{VMM3\textunderscore MASTER \textunderscore GPtemp}.
\section{Configuring the GPVMM Board}
In order to program the GPVMM board, i.e. pass the firmware to its FPGA, one has first to open up \vivado and go to the \e{Hardware Manager}, as can be seen from figure \ref{fig:01}:
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.20]{01.png}
    \caption{Select Hardware Manager}
    \label{fig:01}
\end{figure}

The next step would be to \e{Open Target}, i.e. initialize the communication with the FPGA chip, via JTAG. It is now assumed that the host PC is connected to the board via the programming cable, and that the board is powered-on. Click on \e{Auto-Connect}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.25]{02.png}
    \caption{Open Target}
    \label{fig:02}
\end{figure}
    
\vivado will now try to communicate with the FPGA. If all goes well, then you will see the serial number of the programming cable (\e{xilinx\ud tcf/Digilent/2102499} on figure \ref{fig:03a}) and the FPGA part-ID (\e{xc7a100t\ud 0}) on figure \ref{fig:03a}). On figure \ref{fig:03a}, after right-clicking on the device, the user clicks on \e{Program Device...} which will directly program the FPGA of the board. Note that after loading up the firmware this way, if the board is power-cycled, this firmware will be lost from the FPGA.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.25]{03a.png}
    \caption{Programming the FPGA directly}
    \label{fig:03a}
\end{figure}
 
Then the user should click on the browser button of the pop-up window, and select the appropriate .bin or .bit file. By clicking 'OK', the firmware will be passed to the FPGA; this takes some time to complete, and when the progress bar maxes out, the firmware is in the FPGA, and the user may disconnect the programming cable safely.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.25]{04a.png}
    \caption{Selecting the bitfile}
    \label{fig:04a}
\end{figure}

If the user wishes to load the .bin file which contains the firmware to the flash memory instead, so that the FPGA loads it up after every power-cycle, then the process is a bit different. One has to right click on the FPGA part-ID again, and click on \e{Add Configuration Memory Device} (see figure \ref{fig:03b}).\\

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.25]{03b.png}
    \caption{First step towards programming the flash memory}
    \label{fig:03b}
\end{figure}

After this, the user should select the appropriate memory that is on the GPVMM board via the menu. The device-id is \e{mt25ql256-spi-x1\ud x2\ud x4}, which in other versions of \vivado is indicated as \e{n25q256-3.3v-spi-x1\ud x2\ud x4}. You will find this part easy if you select the appropriate options in the indicated drop-down menus at figure \ref{fig:04b}. Remember that there is a $3.3 \, \mathrm{V}$ memory and a $1.8 \, \mathrm{V}$ one in the list. You must choose the $3.3 \, \mathrm{V}$ memory.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.25]{04b.png}
    \caption{Selecting the correct flash memory}
    \label{fig:04b}
\end{figure}

\newpage
If prompted, select to program the configuration memory device now. The next dialog box will let you browse and select the .bin file you want to load into the flash memory. By clicking 'OK', the firmware will start to get written to the memory. This takes more time than the usual programming. When the process has finalized, the user has to disconnect the programming cable and power-cycle the board, in order for the FPGA to accept the firmware from the board's memory. Be advised that we sometimes have noticed an inability of the FPGA to get configured by the flash memory after a power cycling, if the programming cable is attached to the JTAG connector.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.25]{04d.png}
    \caption{Programming the flash memory}
    \label{fig:04d}
\end{figure}

\section{The VMM Readout/Configuration Firmware}
The firmware allows the user to communicate with the VMM ASIC on the GPVMM board, via the VERSO software. The firmware is compatible with the master branch of the software (v.4.0.0 is one of the stable releases that have been tested).

\subsubsection{CKBC/CKTP, xADC}
The firmware is able to send the CKBC signal to the VMM, which can be configured to be either $40 \, \mathrm{Mhz}$, $20 \, \mathrm{Mhz}$ or $10 \, \mathrm{Mhz}$. It can also send test pulses to the VMM, by asserting the CKTP signal to the VMM's input. The CKTP pulse is a periodic pulse, which can be configured by the user in terms of period, high-time, and skewing with respect to the CKBC. If the user has enabled the appropriate registers in order to activate the test pulsing circuit of each channel in the VMM, then the CKTP will induce charge at that channel, thus creating an event.\\

The firmware also instantiates the xADC module, which can be used for calibration of the VMM.

\subsubsection{Dynamic IP Changing}
The firmware is able to change its IP on-the-fly with the aid of the software, and keep that address over power-cycles. This is achieved by storing the IP address designated by the user in an on-board memory. Upon firmware start-up, the logic reads out the information from that memory, thus retaining the address. By changing the IP address of the board, the user is able to use several boards in the same network, thus allowing for data taking with several boards/VMMs. Be advised that the only IP addresses accepted by the firmware can be of the form of: $192.168.0.x, \, x=2-255$, excluding $x=16$, which should be the host IP address. The default address of the board is $192.168.0.2$. \\

If one wants to use multiple boards in a single network switch, then these boards should have \emph{consecutive} IP addresses. For example, if $3$ boards are used in the DAQ, then these should be their IP addresses: $192.168.0.2$, $192.168.0.3$, $192.168.0.4$.

\subsubsection{VMM Configuration}
The user may configure the VMM through the software and FPGA firmware. The user makes the appropriate selections at the GUI, and by pressing 'Configure', the software sends the configuration bits via UDP to the FPGA, to be picked up by the firmware. The firmware then configures the VMM3 via the SPI protocol. After completing the configuration of the VMM, the FPGA sends a configuration reply packet to the software. The software does not process this reply, but it can be monitored by the user with an appropriate network monitoring tool (e.g. \e{Wireshark}). A configured VMM can yield data to the user. The firmware then must readout the VMM, and forward the data to the software for decoding.

\section{Reading-Out the VMM}
In principle, two readout modes are supported: \e{Continuous}, and \e{Level0}. Each readout mode is implemented by its respective firmware bitfile. At the end of this section, one can find a couple of timing diagrams to assist with the comprehension of the different DAQ modes.

\subsection{Continuous Mode}
The continuous readout mode, which was used in the VMM2, and is still present as a readout option for VMM3, is the most simple readout method of the VMM. Once an event gets buffered into the VMM's FIFOs, it is readily available to be readout by the FPGA. Upon the reception of a trigger signal by the board, either internal (associated with the CKTP), or external (e.g. from scintillators), the FPGA will start querying the VMM for data by asserting the CKTK/CKDT signals. Once the VMM's buffers are empty, the event readout has finalized, and the firmware will send the data via UDP to the VERSO software for decoding. The FPGA will start the readout process, after a fixed latency with respect to the trigger. This latency can be input by the user through the GUI. 

\subsubsection{Continuous Mode - Fixed Window}
In this readout mode, one can operate the DAQ according to the scheme described in the footnote\footnote{\url{https://indico.cern.ch/event/671377/contributions/2746180/attachments/1536788/2408121/fw_bakalis_nsw_elx_171006.pdf}}. In general, the CKBC remains grounded as the VMM is taking data, but upon reception of a trigger, the FPGA sends a finite number of CKBCs to the VMM, so that the VMM processes the input pulses. The number of CKBCs to be sent is configurable through the GUI, and the latency set by the GUI in this mode is the trigger-to-first CKBC pulse latency, measured in steps of $6.25 \, \mathrm{ns}$. 

\subsection{Level0 Mode}
The level0 readout mode, which will be used in the final experiment of ATLAS, is a bit more complicated to implement, as it needs careful configuration by the user. The level0 readout allows the user to cherry-pick data that have a specific BCID, thus allowing for selecting only data of interest, and discard all other data that are associated with e.g. noise. The FPGA asserts the L0 signal to the VMM's input, and the VMM responds with data packets accordingly. The L0 signal is asserted by the FPGA to the VMM, with a latency with respect to the trigger received by the FPGA. This trigger is either generated internally by the CKTP when in pulser mode, or received externally by e.g. a scintillator coincidence when in external mode. This latency can be set at the GUI, and for the L0 mode is measured in units of CKBCs (e.g. if latency = 5 at the GUI, the trigger->L0 assertion latency is $5 \times 25 = 125 \,  \mathrm{ns}$). If the VMM's and the FPGA's configurations are correct, then data will be read out. If the configuration is incorrect, then the VMM will send out empty packets (only headers containing the L0 signal's BCID).\\

The VMM has two registers which should be enabled \e{(sL0ena, sL0enaV)}. In order for the user to read out data via level0 and CKTP assertion, then he/she must do the following: First calculate the difference between the \e{l0offset} VMM register and the \e{rollover} VMM register. Usually the rollover is set to $4095$. The difference between these values should yield a number that must be multiplied by the CKBC's period (usually $25 \, \mathrm{ns}$), thus giving a time value (e.g. if rollover = 4095 and l0offset = 4060, then $4095-4060 = 35 \times 25 \, \mathrm{ns}=875 \, \mathrm{ns}$). Upon receiving a L0 signal from the FPGA, the VMM will "look back" for data at that specific time (in the example given it will go back $875 \, \mathrm{ns}$) . So, in order for the user to get data with CKTP, the latency at the GUI (which in the level0 readout mode is the latency between the assertion of the L0 signal with respect to the trigger assertion), should be equal to that difference, i.e. $35$. In this way, the FPGA will readout the data associated with that CKTP. See also fig. \ref{fig:cktpL0}\\

If the user wants to get data with level0 mode and external trigger, then after calculating the difference between the two aforementioned registers, he/she should take into account the intrinsic latency between the actual trigger assertion with respect to its reception by the board, plus the latency at the GUI. If the sum of these two latencies are about equal with the time value yielded by the \e{l0offset} and \e{rollover} difference then the user should see monitor data coming out of the VMM and FPGA. See also fig. \ref{fig:extL0}\\

Be advised that these are \e{general guidelines} to get data with level0 readout mode. If those two time values are equal or about equal to each other, and the VMM is refusing to give data, then this usually means that the user is out of the window and is pointing towards an incorrect "BCID area" of the VMM. By changing the values of the latency, or by reconfiguring the VMM with different \e{l0offset} values one should try to "find" the data in the VMM's buffers. Also, when one is searching for the data in this mode, it is considered good practice to set the \e{window} VMM register to 7, in order to maximize the window opening width. The user should then decrease the window size, if desired. Last but not least, the \emph{latency} setting of the GUI counts the trigger-to-L0 latency in steps of $25 \, \mathrm{ns}$, but is not exact. It is of course constant for a given design/firmware, but it may vary for a few counts with respect to what the FPGA actually does. Also, if the user is using a slower CKBC frequency, the latency has to be adjusted accordingly, in order to do the matching  (e.g. for $CKBC=20 \, \mathrm{MHz}$ the latency must be doubled - roughly, in order to match the data).\\

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{timingCKTP.png}
	\caption{Example timing diagram of the L0 matching in Pulser mode. Here, latency = $30$, and the rollover and l0offset registers have a difference of $26$. The integration time is $100 \, \mathrm{ns}$, thus shifting the actual pulse forward by $4$ CKBCs with respect to the rising edge of the CKTP, where the latency is being measured from. This configuration will yield the CKTP data correctly.}
	\label{fig:cktpL0}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{timingExt.png}
	\caption{Example timing diagram of the L0 matching in External Trigger mode. At $t_0$, a particle passes through the chamber and a scintillator coincidence occurs. After $200 \, \mathrm{ns}$, the trigger from the coincidence is received by the FPGA. This fixed latency depends on the trigger scheme of each experimental setup. The FPGA then waits for $30$ CKBCs before asserting the L0 signal. The VMM gets the L0, "goes back" by $34$ cycles and matches the data. Here, the latency set at the GUI is $30$, and the \e{rollover} and \e{l0offset} VMM registers have a difference of $34$. The integration time is $100 \, \mathrm{ns}$, thus shifting the actual pulse forward by $4$ CKBCs with respect to $t_0$. This configuration will yield the incident particle data correctly.}
	\label{fig:extL0}
\end{figure}

\section{Other Guidelines and Tips}
The user should also bear in mind that when the FPGA is in ACQ ON state, it can only go to ACQ OFF state and will ignore any other configurations. When the user wishes to configure the VMM, then he/she should do so when the FPGA is in ACQ OFF state. When the user wishes to set different values to the FPGA registers (i.e. change the latency, change the CKTP characteristics etc.), he/she should do so when the FPGA is in ACQ OFF state.\\

The user is advised to ping the board before attempting to operate the software. The default IP is $192.168.0.2$.\\ If the board does not reply, try a broadcast ping command. A useful tool to monitor the transactions between the FPGA and the PC that hosts the software is \e{Wireshark}. The packets that are exchanged between the software and firmware can be seen at this application's GUI, thus giving an overview of the state of the setup (e.g., if the FPGA sends packets of only a few bytes while in ACQ ON, then the VMM does not send out any data).
\newpage