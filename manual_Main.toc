\contentsline {chapter}{\numberline {1}GPVMM Board}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Firmware}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Configuring the GPVMM Board}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}The VMM Readout/Configuration Firmware}{7}{section.2.2}
\contentsline {subsubsection}{CKBC/CKTP, xADC}{8}{section*.2}
\contentsline {subsubsection}{Dynamic IP Changing}{8}{section*.3}
\contentsline {subsubsection}{VMM Configuration}{9}{section*.4}
\contentsline {section}{\numberline {2.3}Reading-Out the VMM}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Continuous Mode}{9}{subsection.2.3.1}
\contentsline {subsubsection}{Continuous Mode - Fixed Window}{9}{section*.5}
\contentsline {subsection}{\numberline {2.3.2}Level0 Mode}{9}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Other Guidelines and Tips}{10}{section.2.4}
\contentsline {chapter}{\numberline {3}Software}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Installation}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Overview}{13}{section.3.2}
\contentsline {chapter}{\numberline {4}Running Procedure}{18}{chapter.4}
\contentsline {section}{\numberline {4.1}Communication}{18}{section.4.1}
\contentsline {section}{\numberline {4.2}Setup}{19}{section.4.2}
\contentsline {chapter}{\numberline {A}VMM3 Registers}{23}{appendix.A}
\contentsline {chapter}{\numberline {B}Useful Links}{25}{appendix.B}
\contentsline {chapter}{\numberline {C}Communication}{26}{appendix.C}
\contentsline {chapter}{\numberline {D}GPVMM3 Schematics}{27}{appendix.D}
\contentsline {chapter}{\numberline {E}CTF Board}{46}{appendix.E}
\contentsline {chapter}{\numberline {F}HDMI Adapter for CTF Board}{53}{appendix.F}
