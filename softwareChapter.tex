\chapter{Software}
This chapter describes the instructions for installing and using the official VMM Ethernet Readout Software (VERSO). The Central DAQ Sofware for configuring a user-set number of VMM-based front end boards in continuous or L0-trigger mode. The software performs UDP based readout and event building. VERSO is a graphical interface with monitoring and calibration capabilities developed alongside the baseline NSW electronics NTUA/BNL firmware.
\section{Installation}
The VERSO Software for the GPVMM board, and all VMM-bearing front-ends is hosted on NSWElectronics GitLab under : \url{https://gitlab.cern.ch/NSWelectronics/vmm_readout_software}.\\

It is strongly recommended to follow the instructions on the Gitlab software, if one has never installed VERSO on his/her host PC. Otherwise, the instructions given below can be followed as well.
\\
The recommended release is v4.4.0 which is for VMM2 and VMM3 readout. To obtain this release do:
\begin{verbatim}
git clone -b v4.4.0 https://gitlab.cern.ch/NSWelectronics/vmm_readout_software.git
\end{verbatim}
The software requirements are:
\begin{itemize}
\item Qt 5.7
\item ROOT 5.34
\item Boost 1.60
\item C++11 (gcc>=4.7)
\end{itemize}
There are a few steps that need to be taken in order for you to obtain, install, and get the DAQ software running. You can follow the instructions giver under the GitLab url mentioned above. \\
\textbf{!} After the installation of these packages it is important for the VERSO to set the environmental parameters of Qt, Boost and ROOT packages under the bashrc of your terminal:
\begin{verbatim}
alias qmake=<path-to-Qt-directory>/Qt/5.7/clang_64/bin/qmake
export DYLD_LIBRARY_PATH=<path-to-boost-directory>/boost/lib:$DYLD_LIBRARY_PATH
export ROOTSYS=<path-to-ROOT-directory>
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$ROOTSYS/lib:$DYLD_LIBRARY_PATH
\end{verbatim}
\newpage
\section{Overview}
After the succesful installation of the VERSO software, the user is ready to start the GUI by running the executable verso file under the VERSO installation path. On Linux machines this executable, by default, will be located in vmm\_readout\_software/build/ . On typical MAC machines this executable tends to be located in vmm\_readout\_software/build/vmmdcs.app/Contents/MacOS/ . Whichever the case may be, the DAQ can be opened up by double-clicking on it from a graphical file-browser or by executing it from within a terminal:
\begin{verbatim}
./<path-to-vmmdcs-executable>/verso
\end{verbatim}
The VERSO interface will start up and figure \ref{versoOverview} is what it looks like. The different areas of VERSO software will be explained below.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.35]{versoOverview}
    \caption{Startup of VERSO interface}
    \label{versoOverview}
\end{figure}
\subsection*{Run Control}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{runControl}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
Displays the number and status of the data acquisition run which is in progress and gives the ability to the user to dump event-indexed ROOT n-tuple and/or raw data file. The user can also start \& stop the readout/run and set VVM2, VMM3, L0 readout and config. 

By pressing calibration, the user can set the run type to calibration. Once the \e{ACQ On} button is hit, while both the \e{Calibration} and \e{Start Run} buttons are pressed, the calibration loop will commence, as it has been configured by the user in the respective tab.  By pressing DataFlow, user opens up VERSO dataflow monitor and by pressing Monitor, user enable sending event samples to VMM monitor (\url {https://gitlab.cern.ch/aikoulou/vmm-mon}).
\end{minipage}
\subsection*{Monitor}
\begin{minipage}{0.6\linewidth}
    \includegraphics[width=\linewidth]{monitor}
\end{minipage}\hfil
\begin{minipage}{0.35\linewidth}
The vmm-mon application is a single window app to monitor the data that VERSO records.
The main panel shows the plots, depending on the settings in the left panel.
\begin{itemize}
\item The list of boards and VMMs is shown, and the user can select any item in the tree to show the corresponding plots in the main panel.
\item  Settings: refresh interval, type of plots to show (PDO, TDO etc),
\item Clear selection in the tree, in order to make the main panel show only VMM plots.
\item Reset histograms (resets all the histograms)
\item  Pause/Resume
\item .png, makes a png snapshot of the main panels and saves to the directory of the vmm-mon.
\end{itemize}
\end{minipage}
\subsection*{Top-level Configuration Parameters}
 \includegraphics[width=\linewidth]{parameters}
In the Config field, the user can load, set and write a configuration xml file with the VMM settings that have been set in the GUI. The procedure to write a new config file is to set the interface parameters, press the "pen" button and specify the directory that user wants to save the config file in. This way, the user has the ability to load the VMM configuration by pressing the folder icon, load the config file and press the "download" button to set the configuration parameters to the VERSO interface. In the Output field, the user can specify the directory location to dump the output files and in the Comment field, the user can comment/text to store as a field inside of ROOT n-tuple. Also, VERSO provides the optional Setup field, where the user can load the DAQ setup configuration such as detector-to-elx channel mapping and detector mapping of detectors and electronics that he/she can find under vmm\_readout\_software/readout\_configuration directory.
\subsection*{Counters}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{counter}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
Displays the total number of triggers (events) recorded and the total numberof unique VMM channel hits recorded.  In Event Stop field, user can set the number of event to record in the run and by setting -1 there is no limit of the events. Be advised that the number of triggers/hits is not exact. One has to open the ntuple after stopping the run to see exactly how many events were recorded by the DAQ system.
\end{minipage}
\subsection*{Communication and Configuration}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{communication}
\end{minipage}\hfil
\begin{minipage}{0.6\linewidth}
By pressing Open Communication, VERSO opens the UDP sockets. In the IPv4 field, the user can specify the IP of the VMM(FEB) board and sets the address of the first FEB and also the number of FEBs to communicate with. In the Configuration Frame, the user can Select the FEB number that he/she wants to send the VMM/FPGA configuration parameters to. In VMM Select, the user specifies the VMM ID  to configure on the selected FEB. By pressing the Configure button, the user sends VMM configuration to the selected FEBs and VMMs. For a successful data acquistion of multiple boards, FEBs ip addresses must be in ascending order, e.g 1st FEB 192.168.0.2 2st FEB 192.168.0.3 etc.
\end{minipage}
\subsection*{Trigger and Acquistion Mode}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{triggerAcq}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
The user is able to change the trigger latency. Also, he/she can set the Acquistion/Trigger Mode by enabling the Pulser (internal) or External VMM trigger mode. A third option given is Fixed Window, where the FPGA can send a specific number of CKBCs upon reception of a trigger. The Latency step-size is measured in steps of $25 \, \mathrm{ns}$ in L0 readout mode, and in units of $6.25  \, \mathrm{ns}$ in continuous/Fixed Window mode. By pressing Set, the user sends the settings to the FPGA.  Via the AQN Off and AQN On, the user can turn off/on VMM acquisition mode. 
\end{minipage}
\subsection*{FPGA clocks Configuration}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{fpga}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
The user can set the maximum number of CKTKs (if in continuous readout mode) and also the frequency of CKBC ($40$,$20$ and $10 \, \mathrm{MHz}$ CKBC frequencies are possible). The CKTP frame is for the configuration of the CKTP (test pulse), where the user sets a fixed number of CKTP pulses to send (-1 for no limit) and also the CKTP skew w.r.t CKBC (1ns steps for $40 \,  \mathrm{ns}$ CKBC clock, 6.25 ms steps otherwise). By pressing Set, the user sends CKTK, CKBC and CKTP configuration to the FEBs.
\end{minipage}
\subsection*{Monitor Sampling and Angle}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{angleMonitor}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
User selects the event sampling rate parameters for sending to vmm-mon application and also the incident angle of detector to be stored in output ROOT n-tuple.
\end{minipage}
\subsection*{FPGA \& VVM Hard Reset}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{hardReset}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
Sends FPGA reset or VMM hard reset command.
\end{minipage}
\subsection*{Global Register I \& II \& Channel Registers}
The VERSO software gives the user the ability to set the Global Configuration Registers of the VMM3. The associated bit names of the registers in Global Registers Tabs are described extensively in the official VMM3 documentation which can be found under NSWElectronics Twiki (\url{https://twiki.cern.ch/twiki/pub/Atlas/NSWelectronics/vmm.pdf}) . Under the Global Register I \& II ,figure \ref{registers} tabs, the user can set most common registers such as Test Pulse amplitude in DAC Counts, Threshold in DAC Counts, Channel Gain, Peak Time, TAC Slope, Channel Polarity, Channel Monitoring etc.  In Channel Registers, figure \ref{channels}, the user can modify the settings of each of the $64$ VMM channels, like masking (SM), or enabling of the test pulse circuitry (ST).
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.42]{registers}
    \caption{Global Registers I \& II Tabs}
    \label{registers}
\end{figure}
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.20]{channels}
    \caption{Channel Registers Tab}
    \label{channels}
\end{figure}
\subsection*{FEB IP Configuration Panel}
\begin{minipage}{0.3\linewidth}
    \includegraphics[width=\linewidth]{ip}
\end{minipage}\hfil
\begin{minipage}{0.55\linewidth}
Under the Set IP tab, the user can change the default ip of the FEB board (default: 192.168.0.2) to a new board ip. By pressing Set, the user sends the IP configuration to the FPGA. After the change of ip, it is mandatory for the user to re-set the IPv4 field with the new FEB's ip under the Communication Frame of VERSO software.
\end{minipage}
\subsection*{Calibration}
Under Calibration Tab, the user can perform calibration routines using the FPGA's xADC or in non-xADC moed. In xADC based sampling calibration type, the user can perform calibration routines to sample:
\begin{itemize}
\item Analog DAC levels (threshold and pulser)
\item Channel-by-channel threshold variations, stepping over the VMM threshold trimmers values
\item Channel baseline \& noise levels
\end{itemize}
An important remark here is the fact that the calibration loop may "freeze", especially in the baseline xADC readout mode. This happens if the voltage sampled by the xADC is above a certain limit. For the baseline runs, this happens if a channel is dead. The loop will then stop, and the user has to press the \e{Manual xADC configuration} button in the GUI to have VERSO ignore the channel and move on. The user can realize if the loop has frozen by looking at the \e{Ch. Monitor} field of the Global Registers 1 tab. The number of monitored channel for the baseline check should always be incrementing. See also \ref{subsec:xadc}\\

In non-xADC based sampling calibration type, the user can perform calibration routines based on the loop parameters at the bottom of the window (Gain, Peak Time, TAC Slope, Test Pulse DAC etc.) Further information for the calibration techniques can be found under Daniel Antrim's presentation (\url{https://indico.cern.ch/event/671377/contributions/2746181/attachments/1536796/2407884/2017\_10\_6\_verso\_and\_calibration_overviewPDF.pdf}). Also, a calibration software has also been developed to perform calibration analysis on the output of the calibration runs from VERSO software and can be found under NSWElectronics GitLab (\url{https://gitlab.cern.ch/NSWelectronics/vmm_calibration_software}).
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.42]{calibration}
    \caption{Calibration Tab}
    \label{registers}
\end{figure}

\newpage

\chapter{Running Procedure}
This chapter describes the right steps for succesful data acquisition either with internal trigger (pulser) and external  trigger (scintillator).
\section{Communication}
The very first step for the right communication with the board is to set static IPv4 of the desktop's network card with the below network settings:
\begin{itemize}
\item IP Address : 192.168.0.16
\item  Subnet Mask : 255.255.255.0
\end{itemize}
After the setup of network settings, the user plugs the GPVMM board in the desktop PC through an Ethernet cable and supplies the board with $+3.3 \, \mathrm{V}$ or $+3.4 \, \mathrm{V}$ to $+42 \, \mathrm{V}$ DC-DC (selected by power swich and jumpers on the board). In newer versions of the firmware, a short LED blinking pattern will occur at the on-board USER\_ LEDs, if the reference clock is being received correctly by the FPGA. The user can check the communication with the board by pinging the board's IP address (default IP: 192.168.0.2) :
\begin{verbatim}
ping 192.168.0.2
\end{verbatim}
Also, the user can find the IP of the board by broadcast ping:
\begin{verbatim}
ping -b 192.168.0.255
\end{verbatim}
If the ping is succesful and the Ethernet LEDs are blinking, the user can continue with the experiment setup. If not, the user should connect GPVMM board to CTF board via uHDMI to get the reference clock, once the connection is succesful the D17 LED ("PLL/MMCM Lock" signal) will turn on and Ethernet LEDs will start to blink. In newer version of the firmware the LED pattern will also be visible for the first few seconds.
\\
Also, it is mandatory to mention that there are two different versions of the GPVMM board's firmware, with reference clock via uHDMI/CTF and another without the external reference clock. The default firmware of GPVMM boards needs CTF reference clock. User should follow Chapter 3 to program GPVMM with the right firmware.
\newpage
\section{Setup}
The experiment setup for data acquisition using GPVMM board is shown in figure \ref{setup} below. The setup consists of the user's detector, PC for the communication with GPVMM board through ethernet cable, oscilloscope for the monitor output of the VMM channels, power supply, scintillators for the trigger mode(TTL pulses) and also a fan is appropriate for the cooling of VMM and FPGA chip. Also, the use of a CTF board for reference clock is optional and it depends on the GPVMM's firmware. The user in order to use the reference clock from CTF board must not connect GPVMM board directly to CTF via uHDMI, but one must use a compatible HDMI adapter which has been designed by the NTUA. Additional information for the HDMI adapter board can be found at Appendix F.
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.34]{setup}
    \caption{Experimental Setup}
    \label{setup}
\end{figure}
\\
\subsection*{Internal Trigger (Pulser)} 
Τhe steps below describe the procedure the user must follow to be able to record data with the Internal Trigger Mode (Pulser):
\begin{itemize}
\item[1.] Start VERSO software 
\item[2.] Set board's IP in IPv4 field
\item[3.] Press Open Communication button
\item[4.] Set Global Register's 1 \& 2 parameters
\item[5.] Set Internal Pulse[ST] or Mask[SM] channels through Channel Registers Tabs
\item[6.] Select Pulser Mode In Mode Frame
\item[7.] Press VMM3 Hard Reset button
\item[8.] Press Set button in CKTP Frame
\item[9.] Press Set button in Mode Frame
\item[10.] Press Configure button to sent the configuration parametes to VMM
\end{itemize}
\subsection*{External Trigger} 
Τhe steps below describe the procedure the user must follow to be able to record data with the External Trigger Mode (e.g scintillator):
\begin{itemize}
\item[1.] Connect Scintillator Coincidence output to TRG\_IN port\footnote{This is true if the firmware loaded supports external triggering through the lemo connector. There are some versions of the firmware that support only trigger through the uHDMI/miniSAS via the CTF board.}.
\item[2.] Start VERSO software 
\item[3.] Set board's IP in IPv4 field
\item[4.] Press Open Communication button
\item[5.] Set Global Register's 1 \& 2 parameters
\item[6.] Mask[SM] channels through Channel Registers Tabs (optional)
\item[7.] Select External Mode In Mode Frame
\item[8.] Set trigger parameters (latency, lat.extra) in Trigger Frame
\item[9.] Press VMM3 Hard Reset button
\item[10.] Press Set button in CKTP Frame
\item[11.] Press Set button in Mode Frame
\item[12.] Press Configure button to sent the configuration parametes to VMM
\end{itemize}
\subsection*{Acquisition and Monitor Guidelines} 
After following the steps above, the user is ready to start the acquisition of data by pressing ACQ On in Acquistion Frame as well as record them in a ROOT n-tuple by pressing Start Run. If the procedure is successful, the counter of Triggers and Hits will start to count the number of triggers and hits that user is recording. If the counter is not incrementing, then it can either be that the VMM/FPGA is not receiving triggers at all, or that the VMM is receiving triggers/l0 signals, but no events are being recorded. At any given time, the user can stop the recording procedure by pressing ACQ\_ Off and Stop Run. The ROOT n-tuple file is placed under the directory that the user had specified in the Output field. For every change in the VERSO parameters and registers, it is mandatory to repeat steps 9-12 for the successful sending of registers to the VMM and FPGA. \\

The user is recommended to use an oscilloscope connected to the GPVMM3 board through a LEMO cable in MO ouput, to monitor the VMM channel output pulse, an example of internal pulser shown in figure , which have selected to monitor by the Ch. Monitor [sm5-sm0] selection in  Global Registers 1 tab in VERSO software. Otherwise, a useful tool to monitor the transactions between the GPVMM board and the PC that hosts the software is \e{Wireshark}. The packets that are exchanged between the software and firmware can be seen at this application's GUI, thus giving an overview of the state of the setup (e.g., if the FPGA sends packets of only a few bytes while in ACQ ON, then the VMM does not send out any data).
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7,width=350pt]{pulseVMM}
    \caption{Monitor Output of VMM channel through Internal Trigger Mode (Pulser)}
\end{figure}

\subsection*{xADC Calibration Run}\label{subsec:xadc}
This short subsection gives information on how to run a baseline and threshold DAC calibration loop.
\subsubsection*{Baselines}
\begin{itemize}
	\item[1.] Start VERSO software 
	\item[2.] Set board's IP in IPv4 field
	\item[3.] Press Open Communication button
	\item[4.] Set Global Register's 1 \& 2 parameters
	\item[5.] Go to Calibration Tab
	\item[6.] Select xADC, select which VMMs to sample from (one for GPVMM, eight for MMFE8)
	\item[7.] Select number of samples for each calibration step and the channel range
	\item[8.] Select Baselines in the list on the left
	\item[9.] Select Calibration at the top left of the VERSO GUI, and press ACQ\_ On at Global Registers 1
	\item[10.] Once the run finishes (the Stop Run should not be clickable anymore, and the Channel Monitor should have stopped rolling at channel 63, if the final channel selected at the calibration tab was 63), one can open the calibration file.
	\item[11.] If the calibration loop hangs at a channel, one should press the Manual xADC Configuration button at the Calibration Tab once, and VERSO will sample over the next channel.
	\item[12.] If the FPGA is sending calibration packets to VERSO and the loop proceeds normally, but VERSO writes an empty .ROOT file, then the user should close and restart VERSO without any config .xml loaded.
\end{itemize}

The command in the .ROOT prompt to see the baseline voltage profile across an entire eight-VMM board is (e.g. MMFE8):
\begin{verbatim}
calib->Draw("samples/4.096:channel+chip*64>>h1(512,0,511, 4096, 0, 4095)", "", "colz")
\end{verbatim}
And for a 1-VMM board (e.g. GPVMM):
\begin{verbatim}
calib->Draw("samples/4.096:channel>>h1(64,0,63, 4096, 0, 4095)", "", "colz")
\end{verbatim}

\subsubsection*{Threshold DAC}
\begin{itemize}
	\item[1.] Start VERSO software 
	\item[2.] Set board's IP in IPv4 field
	\item[3.] Press Open Communication button
	\item[4.] Set Global Register's 1 \& 2 parameters
	\item[5.] Go to Calibration Tab
	\item[6.] Select xADC, select which VMMs to sample from (one for GPVMM, eight for MMFE8)
	\item[7.] Select number of samples for each calibration step and the channel range
	\item[8.] Select Threshold DAC in the list on the left
	\item[9.] Select Threshold DAC scan range, and step size
	\item[10.] Select Calibration at the top left of the VERSO GUI, and press ACQ\_ =On at Global Registers 1
	\item[11.] Once the run finishes (the Stop Run should not be clickable anymore, and the Threshold DAC should have stopped rolling at the maximum value, as set by the user two steps before.
	\item[11.] If the calibration loop hangs, one should press the Manual xADC Configuration button at the Calibration Tab.
	\item[12.] If the FPGA is sending calibration packets and the loop proceeds normally, but VERSO writes an empty .ROOT file, then the user should close and restart VERSO without any config .xml loaded.
\end{itemize}

The command in the .ROOT prompt to see the threshold DAC voltage for chip 0 of a given board is:
\begin{verbatim}
calib->Draw("samples/4.096:dacCounts>>h1(1024,0,1023, 4096, 0, 4095)", "chip==0", "colz")
\end{verbatim}
Obviously one can change the chip number to see the DAC->Voltage relation for all chips if a multi-chip board is used. For the GPVMM board, the above command is sufficient.

\appendix
\chapter{VMM3 Registers}
The table below includes the Global Configuration Registers of the VMM:
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7,width=350pt]{globalRegisters}
    \caption{Global Configuration Registers of the VMM}
\end{figure}
\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7,width=350pt]{extraGlobalRegisters}
    \caption{Global Configuration Registers of the VMM (continued)}
\end{figure}
\newpage
The below table includes the Channel Configuration Registers of the VMM:
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.7,width=350pt]{channelRegisters}
    \caption{Channel Configuration Registers of the VMM}
\end{figure}
\newpage 



 